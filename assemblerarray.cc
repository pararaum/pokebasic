#include "assemblerarray.hh"
#include <algorithm>
#include <sstream>

AssemblerArray &AssemblerArray::operator+=(const AssemblerArray &other) {
  std::copy(other.begin(), other.end(), std::back_inserter(*this));
  return *this;
}

AssemblerArray &AssemblerArray::mnemonic(const std::string &op) {
  push_back(move(std::make_unique<Mnemonic>(op)));
  return *this;
}
AssemblerArray &AssemblerArray::mnemonic(const std::string &op, const std::string &par) {
  push_back(move(std::make_unique<Mnemonic>(op, par)));
  return *this;
}
AssemblerArray &AssemblerArray::comment(const std::string &cmt) {
  push_back(move(std::make_unique<Comment>(cmt)));
  return *this;
}
AssemblerArray &AssemblerArray::label(const std::string &lab) {
  push_back(move(std::make_unique<Label>(lab)));
  return *this;
}
AssemblerArray &AssemblerArray::jmp(const std::string &operand) {
  mnemonic("JMP", operand);
  return *this;
}
AssemblerArray &AssemblerArray::jsr(const std::string &operand) {
  mnemonic("JSR", operand);
  return *this;
}
AssemblerArray &AssemblerArray::lda(const std::string &operand) {
  mnemonic("LDA", operand);
  return *this;
}
AssemblerArray &AssemblerArray::ldai(uint8_t x) {
  mnemonic("LDA", '#' + std::to_string(x));
  return *this;
}
AssemblerArray &AssemblerArray::ldx(const std::string &operand) {
  mnemonic("LDX", operand);
  return *this;
}
AssemblerArray &AssemblerArray::ldxi(uint8_t x) {
  mnemonic("LDX", '#' + std::to_string(x));
  return *this;
}

AssemblerArray &AssemblerArray::ldyi(uint8_t x) {
  mnemonic("LDY", '#' + std::to_string(x));
  return *this;
}

AssemblerArray &AssemblerArray::pha() {
  mnemonic("PHA");
  return *this;
}
AssemblerArray &AssemblerArray::pla() {
  mnemonic("PLA");
  return *this;
}
AssemblerArray &AssemblerArray::sta(const std::string &operand) {
  mnemonic("STA", operand);
  return *this;
}
AssemblerArray &AssemblerArray::sta(unsigned int operand) {
  std::ostringstream out;
  out << '$' << std::hex << operand;
  mnemonic("STA", out.str());
  return *this;
}
AssemblerArray &AssemblerArray::stx(const std::string &operand) {
  mnemonic("STX", operand);
  return *this;
}
AssemblerArray &AssemblerArray::tay() {
  mnemonic("TAY");
  return *this;
}
AssemblerArray &AssemblerArray::tya() {
  mnemonic("TYA");
  return *this;
}
