#include "assemblerstatement.hh"

std::string Comment::str() const {
  return ";;;\t" + comment;
}

std::string Label::str() const {
  return label + ':';
}

std::string Mnemonic::str() const {
  auto mnm = '\t' + mnemonic;
  if(!operand.empty()) {
    mnm += '\t' + operand;
  }
  return  mnm;
}
