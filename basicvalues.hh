#ifndef __BASICVALUES_HH_20240414__
#define __BASICVALUES_HH_20240414__
#include "assemblerarray.hh"
#include "peglib.h"

AssemblerArray peekvalue(const peg::SemanticValues &vs);
  
#endif
