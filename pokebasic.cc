#include <iostream>
#include <sstream>
#include <set>
#include <memory>
#include <functional>
#include <assert.h>
#include <boost/format.hpp>
#include "peglib.h"
#include "basicvalues.hh"

using namespace std;

class Compiler {
protected:
  unsigned int idgen;
  AssemblerArray code;
  string prelude = R"(
	.include	"LAMAlib.inc"
	.segment	"STARTUP"
	jsr	BASICbegin
	rts
	.segment	"INIT"
	.segment	"ONCE"
	
	.zeropage
ptr1:	.res	2
tmp1:	.res	1
tmp2:	.res	1
tmp3:	.res	1
tmp4:	.res	1
	.zeropage
expacc:	.res	2

)";
public:
  struct LoopPars {
    string varname;
    string looplabel;
  };
  vector<LoopPars> loop_stack; //!< Stack for balancing loop variables.
  set<string> variables; //!< List of all variables used.
  
  int line_number; //!< Current line number.

  //! Constructor
  Compiler() : idgen(999), line_number(-1) {}
  //! Get an id for labels
  int get_id() { return idgen++; }
  //! Get an id for labels as an string
  string get_id_str() {
    boost::format fid("%04X");
    return str(fid % get_id());
  }
  void add_code(const AssemblerArray &morecode) {
    code += morecode;
  }
  ostream &output_code(ostream &out) const {
    if(!loop_stack.empty()) {
      ostringstream err;
      err << "missing next (";
      for_each(loop_stack.rbegin(),
	       loop_stack.rend(),
	       [&err](auto const &x) {
		 err << x.varname << ' ';
	       });
      err << "still on stack)";
      throw logic_error(err.str());
    }
    out << prelude << endl;
    out << "\t.data\n";
    for(auto i : variables) {
      cout << i << ":\t.word 0\n";
    }
    out << "\t.code\n"
	<< "BASICbegin:\n";
    for(auto &i : code) {
      out << i->str() << endl;
    }
    out << "\trts\n";
    return out;
  }
};


int main(int argc, char **argv) {
  Compiler compiler;
  peg::parser parser(R"(
  	# Simplified BASIC grammar
	## These should return lines of code.
	lines <- (line)*
	line <- NUMBER statements '\n'
	statements <- statement (STATEMENTSEPARATOR statement)*
	statement <- pokestatement / forstatement / nextstatement
  	pokestatement <- POKE NUMBER ',' expression / POKE expression ',' expression
  	forstatement <- FOR VARNAME '=' NUMBER 'to'i NUMBER
	nextstatement <- NEXT VARNAME?
	expression <- term ( OPERATOR term)* { precedence L - + L * }
	term <- NUMBER / peekvalue / VARNAME / '(' expression ')'
	#value <- peekvalue
	peekvalue <- PEEK '(' expression ')'
	# Terminal symbols
	## These may return different things.
  	NUMBER <- < [0-9]+ >
	VARNAME <- < [a-zA-Z][0-9a-zA-Z]* >
	PEEK <- 'peek'i
	POKE <- 'poke'i
	FOR <- 'for'i
	NEXT <- 'next'i
	OPERATOR <- [-+*]
	STATEMENTSEPARATOR <- ':'
	# Other
  	%whitespace <- [ \t]*
  )");
  // Check if grammer is ok.
  assert(static_cast<bool>(parser));
  // Add a function for outputting errors.
  parser.set_logger([](size_t line, size_t col, const string& msg) {
		      cerr << "Error: " << line << ":" << col << ": " << msg << "\n";
		    });

  parser["lines"] =
    [&compiler](const peg::SemanticValues &vs, any &dt) {
      for(auto i : vs) {
  	try {
  	  compiler.add_code(any_cast<AssemblerArray>(i));
  	}
  	catch(const std::bad_any_cast &e) {
  	  cerr << "Error converting " << vs.sv() << endl;
  	  throw;
  	}
      }
    };
  parser["line"] =
    [&compiler](const peg::SemanticValues &vs) {
      try {
  	AssemblerArray code;
  	code.comment("line " + to_string(any_cast<int>(vs[0])));
  	compiler.line_number = any_cast<int>(vs[0]);
  	code += any_cast<AssemblerArray>(vs[1]);
  	return code;
      }
      catch(const std::bad_any_cast &e) {
  	cerr << "Error converting " << vs.sv() << endl;
  	throw;
      }
  };
  parser["statements"] =
    [&compiler](const peg::SemanticValues &vs) {
      AssemblerArray ass;
      for(unsigned i = 0; i < vs.size(); ++i) {
  	try {
  	  AssemblerArray stmt(any_cast<AssemblerArray>(vs[i]));
  	  ass.comment(str(boost::format("statement %p") % &vs));
  	  ass += stmt;
  	}
  	catch(const bad_any_cast &e) {
  	  cerr << "Bad any cast on " << vs.sv()
  	       << " i=" << i
  	       << " line " << compiler.line_number
  	       << endl;
  	  throw;
  	}
      }
      return ass;
    };
  parser["forstatement"] =
    [&compiler](const peg::SemanticValues &vs) {
      string varname = any_cast<string>(vs[1]);
      int from = any_cast<int>(vs[2]);
      int to = any_cast<int>(vs[3]);
      string looplabel = "LOOP_" + compiler.get_id_str();
      AssemblerArray code;
      Compiler::LoopPars lp{varname, looplabel};

      compiler.variables.insert(varname);
      compiler.loop_stack.push_back(lp);
      code.mnemonic("ldax", '#' + to_string(from)).mnemonic("stax", varname);
      code.label(looplabel);
      code.mnemonic("ldax", varname).
        mnemonic("cmpax", '#' + to_string(to)).
        mnemonic("bcc", looplabel + "_cont").
        mnemonic("beq", looplabel + "_cont").
        jmp(looplabel + "_out").
        label(looplabel + "_cont");
      return code;
    };
  parser["nextstatement"] =
    [&compiler](const peg::SemanticValues &vs) {
      string varname;
      AssemblerArray code;
      Compiler::LoopPars lpars;

      if(compiler.loop_stack.empty()) {
  	throw underflow_error("no for for next in line " + to_string(compiler.line_number));
      } else {
	lpars = compiler.loop_stack.back();
  	if((vs.size() > 0) && (any_cast<string>(vs[1]) != lpars.varname)) {
  	  throw invalid_argument("for loop variable mismatch in line " + to_string(compiler.line_number));
  	}
      }
      code.mnemonic("inc16", lpars.varname);
      code.jmp(compiler.loop_stack.back().looplabel);
      code.label(lpars.looplabel + "_out");
      compiler.loop_stack.pop_back();
      return code;
    };
  parser["pokestatement"] =
    [](const peg::SemanticValues &vs) {
      AssemblerArray code;
      switch(vs.choice()) {
      case 0:
  	code += any_cast<AssemblerArray>(vs[2]);
  	{
  	  int destination = any_cast<int>(vs[1]);
  	  code.sta(destination);
  	}
  	return code;
  	break;
      case 1:
  	code += any_cast<AssemblerArray>(vs[2]);
  	code.pha();
  	code += any_cast<AssemblerArray>(vs[1]);
  	code.sta("ptr1");
  	code.stx("ptr1+1");
  	code.pla();
  	code.mnemonic("ldy", "#0");
  	code.mnemonic("sta", "(ptr1),y");
  	return code;
      default:
  	throw logic_error("choice() in PokeStatement");
      }
    };
  parser["expression"] =
    [&compiler](const peg::SemanticValues &vs) {
      AssemblerArray code;
      try {
  	// Save the expression accumulator on the stack.
  	code.lda("expacc").pha().lda("expacc+1").pha();
  	// Get the first term into AX.
  	code += any_cast<AssemblerArray>(vs[0]);
  	// And save it to the expression accumulator.
  	code.mnemonic("stax", "expacc");
  	for(unsigned i = 1; i < vs.size(); i += 2) {
  	  // Get the next expression into AX.
  	  try {
  	    code += any_cast<AssemblerArray>(vs[i + 1]);
  	  }
  	  catch(const std::bad_any_cast &e) {
  	    cerr << "any_cast<string>(vs[i + 1]): " << e.what() << endl;
  	  }
  	  switch(any_cast<char>(vs[i])) {
  	  case '+':
  	    // Add AX to expacc.
  	    code.mnemonic("clc").mnemonic("adcax", "expacc").mnemonic("stax", "expacc");
  	    break;
  	  case '-':
  	    // Subtract AX from expacc;
  	    code.
  	      mnemonic("stax", "tmp1"). // Temporarily store AX.
  	      mnemonic("ldax", "expacc"). // Get expression accumulator in AX.
  	      mnemonic("sec").mnemonic("subax", "tmp1"). // Now subtract.
  	      mnemonic("stax", "expacc"); // And store in the accumulator.
  	    break;
  	  case '*':
  	    code.mnemonic("mul16", "expacc")
	      .mnemonic("stax", "expacc");
  	    break;
  	  default:
  	    throw logic_error("unknown operator");
  	  }
  	}
	// AX has the result at this point.
	code.tay(); // Save accumulator.
	// Restore the expression accumulator on the stack.
  	code.pla().sta("expacc+1").pla().sta("expacc");
	code.tya(); // Restore accumulator.
      }
      catch(const std::bad_any_cast &e) {
  	cerr << "Bad any_cast in line " << compiler.line_number << endl;
  	throw;
      }
      return code;
    }; 
  parser["term"] =
    [](const peg::SemanticValues &vs, any &dt) {
      try {
	AssemblerArray code;
	switch(vs.choice()) {
	case 0:
	  {
	    int val = any_cast<int>(vs[0]);
	    code.ldai(val & 0xFF).ldxi(val / 256);
	  }
	  break;
	case 1:
	  code += any_cast<AssemblerArray>(vs[0]);
	  break;
	case 2:
	  {
	    string var = any_cast<string>(vs[0]);
	    code.lda(var);
	    code.ldx(var + "+1");
	  }
	  break;
	case 3:
	  code += any_cast<AssemblerArray>(vs[0]);
	  break;
	default:
	  throw logic_error("choice()=" + to_string(vs.choice()) + " in Expression");
	}
	return code;
      }
      catch(...) {
   	cerr << "Error converting " << vs.sv() << endl;
   	throw;
      }
    };
  // Values...
  parser["peekvalue"] = &peekvalue;
  //
  parser["NUMBER"] =
    [](const peg::SemanticValues &vs) {
      return vs.token_to_number<int>();
    };
  parser["VARNAME"] =
    [&compiler](const peg::SemanticValues &vs) {
      string varnam{"BASICvar_" + vs.token_to_string()};
      compiler.variables.insert(varnam);
      return varnam;
    };
  parser["OPERATOR"] =
    [](const peg::SemanticValues &vs, any &dt) {
      return static_cast<char>(*vs.sv().data());
    };
  // Prepare parse
  parser.enable_packrat_parsing(); // Enable packrat parsing.
  // Initialise Compiler
  any anycompiler = make_any<Compiler*>(&compiler);
  // Read into string.
  ostringstream contents;
  contents << cin.rdbuf();
  // Parse!
  auto ret = parser.parse(contents.str(), anycompiler);
  if(!ret) {
    return 1;
  }
  try {
    compiler.output_code(cout);
  }
  catch(const exception &excp) {
    cerr << "Error: " << excp.what() << endl;
    return 5;
  }
  return 0;
}
