#include "assemblerarray.hh"
#include "basicvalues.hh"

AssemblerArray peekvalue(const peg::SemanticValues &vs) {
  AssemblerArray code;
  // PEEK ( expression )
  // vs[0]  vs[1]
  code += std::any_cast<AssemblerArray>(vs[1]);
  code.mnemonic("stax", "ptr1"). // Store address in pointer.
    ldyi(0).
    mnemonic("lda", "(ptr1),y").
    ldxi(0);
  return code;
}

