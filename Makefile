#! /usr/bin/make -f

CXXFLAGS=-std=c++17 -O3 -DNDEBUG -Wall

TARGET=pokebasic
OBJS=assemblerstatement.o assemblerarray.o basicvalues-peekvalue.o

$(TARGET): $(TARGET).o $(OBJS)
	g++ -pthread $(CXXFLAGS) -o $@ $^

%.o:	%.cc
	$(CXX) -c -pthread -Icpp-peglib $(CXXFLAGS) -o $@ $^

.PHONY:	clean
clean:
	rm -f $(TARGET) *.o

