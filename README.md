# PokeBASIC #

A very simple BASIC compiler for the C64 proof of concept. This
compiler will generate an assembler source from a basic program in a
dialect of the Commodore BASIC V2. Only POKE, FOR, and NEXT are
supported, all variables are 16 bit unsigned and expressions can
contain `+`, `-`, and `*`. Parenthesis are allowed, too.

For the compiler yhirose's phantastic packrat parser library is used.

## Building ##

Prerequisites are the boost library and a c++ compiler with c++17
support.

For building the executable a simple `make` should do. Do not forget
to use `git submodule init` and `git submodule update` after cloning
in order to have a copy of the peglib.

## Usage ##

Using `pokebasic < <inputfile>` an assembler source code is outputted
to the standard output. It should be redirected into a file and
compiled using `cl65`, you need to have the LAMAlib installed.

For convenience you can use the Docker image at
https://hub.docker.com/r/vintagecomputingcarinthia/c64build
for the C64 C compiler/assembler.

# Links #

  * https://hub.docker.com/r/vintagecomputingcarinthia/cc65forvcc
  * https://github.com/demesos/LAMAlib
  * https://github.com/yhirose/cpp-peglib
