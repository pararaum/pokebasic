#ifndef __ASSEMBLERARRAY_HH_20240414__
#define __ASSEMBLERARRAY_HH_20240414__
#include <vector>
#include <string>
#include "assemblerstatement.hh"


class AssemblerArray : public std::vector<AssemblerStatementPtr> {
public:
  AssemblerArray &operator+=(const AssemblerArray &other);
  AssemblerArray &mnemonic(const std::string &op);
  AssemblerArray &mnemonic(const std::string &op, const std::string &par);
  AssemblerArray &comment(const std::string &cmt);
  AssemblerArray &label(const std::string &lab);
  AssemblerArray &jmp(const std::string &operand);
  AssemblerArray &jsr(const std::string &operand);
  AssemblerArray &lda(const std::string &operand);
  AssemblerArray &ldai(uint8_t x);
  AssemblerArray &ldx(const std::string &operand);
  AssemblerArray &ldxi(uint8_t x);
  AssemblerArray &ldyi(uint8_t x);
  AssemblerArray &pha();
  AssemblerArray &pla();
  AssemblerArray &sta(const std::string &operand);
  AssemblerArray &sta(unsigned int operand);
  AssemblerArray &stx(const std::string &operand);
  AssemblerArray &tay();
  AssemblerArray &tya();
};

#endif
