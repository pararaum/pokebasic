#ifndef __ASSEMBLERSTATEMENT_HH_20240414__
#define __ASSEMBLERSTATEMENT_HH_20240414__
#include <string>
#include <memory>

class AssemblerStatement {
public:
  virtual std::string str() const = 0;
};


class Comment : public AssemblerStatement {
protected:
  std::string comment;
public:
  Comment(const std::string &com) : comment(com) {}
  virtual std::string str() const;
};


class Label : public AssemblerStatement {
protected:
  std::string label;
public:
  Label(const std::string &l) : label(l) {}
  virtual std::string str() const;
};


class Mnemonic : public AssemblerStatement {
protected:
  std::string mnemonic;
  std::string operand;
public:
  Mnemonic(const std::string &m) : mnemonic(m) {}
  Mnemonic(const std::string &m, const std::string &o) : mnemonic(m), operand(o) {}
  virtual std::string str() const;
};


// If using unique_ptr we will only get trouble with move semantics.
typedef std::shared_ptr<AssemblerStatement> AssemblerStatementPtr;

#endif
